<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 25-07-15
 * Time: 09:00 PM
 */

include "Persona.php";

class Alumno extends Persona
{
    private $_idAlumno;
    private $_rutAlumno;


    /**
     * @return mixed
     */
    public function getRutAlumno()
    {
        return $this->_rutAlumno;
    }

    /**
     * @param mixed $_rutAlumno
     */
    public function setRutAlumno($_rutAlumno)
    {
        $this->_rutAlumno = $_rutAlumno;
    }

    /**
     * @return mixed
     */
    public function getIdAlumno()
    {
        return $this->_idAlumno;
    }

    /**
     * @param mixed $idAlumno
     */
    public function setIdAlumno($idAlumno)
    {
        $this->_idAlumno = $idAlumno;
    }

}