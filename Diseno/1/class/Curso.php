<?php
/**
 * Created by PhpStorm.
 *
 * Clase Curso
 *
 * User: Victor
 * Date: 25-07-15
 * Time: 09:04 PM
 */

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

include_once "Alumno.php";

/**
 * Class Curso
 */
class Curso {
    /**
     * @var
     */
    private $_nombreCurso;
    /**
     * @var array
     */
    private $_alumnosCurso;
    /**
     * @var
     */
    private $_profesor;


    /**
     *
     */
    function __construct() {
        $this->_alumnosCurso=array();
    }

    /**
     * @return mixed
     */
    public function getNombreCurso()
    {
        return $this->_nombreCurso;
    }
    /**
     * @param mixed $_nombreCurso
     */
    public function setNombreCurso($_nombreCurso)
    {
        $this->_nombreCurso = $_nombreCurso;
    }

    /**
     * @return array
     */
    public function getAlumnosCurso(){
        return $this->_alumnosCurso;
    }

    /**
     * @param Alumno $alumnosCurso
     * @return Alumno
     */
    public function setAlumnosCurso(Alumno $alumnosCurso){
        return $this->_alumnosCurso[]=$alumnosCurso;
    }

    /**
     * @param Profesor $profesor
     */
    public function setProfesor(Profesor $profesor){
        $this->_profesor=$profesor;
    }

    /**
     * @param $idCurso
     * @return mixed
     */
    public function getProfesor($idCurso){
        return $this->_profesor;
    }

}