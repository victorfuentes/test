<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 25-07-15
 * Time: 09:13 PM
 */

class Resultado {

    private $_nota;
    private $_idAlumno;
    private $_prueba;


    function __construct() {

    }


    /**
     * @return mixed
     */
    public function getIdAlumno()
    {
        return $this->_idAlumno;
    }

    /**
     * @param mixed $idAlumno
     */
    public function setIdAlumno($idAlumno)
    {
        $this->_idAlumno = $idAlumno;
    }

    /**
     * @return mixed
     */
    public function getPrueba()
    {
        return $this->_prueba;
    }

    /**
     * @param mixed $prueba
     */
    public function setPrueba(Prueba $prueba)
    {
        $this->_prueba= $prueba;
    }

    /**
     * @return mixed
     */
    public function getNota()
    {
        return $this->_nota;
    }

    /**
     * @param mixed $nota
     */
    public function setNota($nota)
    {
        $this->_nota = $nota;
    }



}