<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 25-07-15
 * Time: 09:05 PM
 */

class Prueba {
    private $_idPrueba;
    private $_nombrePrueba;
    private $_idCurso;

    /**
     * @return mixed
     */
    public function getNombrePrueba()
    {
        return $this->_nombrePrueba;
    }

    /**
     * @param mixed $_nombrePrueba
     */
    public function setNombrePrueba($_nombrePrueba)
    {
        $this->_nombrePrueba = $_nombrePrueba;
    }

    /**
     * @return mixed
     */
    public function getIdCurso()
    {
        return $this->_idCurso;
    }

    /**
     * @param mixed $idCurso
     */
    public function setIdCurso($idCurso)
    {
        $this->_idCurso = $idCurso;
    }

    /**
     * @return mixed
     */
    public function getIdPrueba()
    {
        return $this->_idPrueba;
    }

    /**
     * @param mixed $isPrueba
     */
    public function setIdPrueba($idPrueba)
    {
        $this->_idPrueba = $idPrueba;
    }


}