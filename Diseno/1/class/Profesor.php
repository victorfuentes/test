<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 25-07-15
 * Time: 09:00 PM
 */
include_once "Persona.php";

class Profesor extends Persona{

    private $_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }
    public function setId($id)
    {
        $this->_id=$id;
    }

}