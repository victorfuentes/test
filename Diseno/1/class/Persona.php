<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 25-07-15
 * Time: 08:53 PM
 */

class Persona {

    private $_nombre;
    private $_apellidoPaterno;
    private $_apellidoMaterno;

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->_nombre;
    }

    /**
     * @param mixed $_nombre
     */
    public function setNombre($_nombre)
    {
        $this->_nombre = $_nombre;
    }

    /**
     * @return mixed
     */
    public function getApellidoPaterno()
    {
        return $this->_apellidoPaterno;
    }

    /**
     * @param mixed $_apellidoPaterno
     */
    public function setApellidoPaterno($_apellidoPaterno)
    {
        $this->_apellidoPaterno = $_apellidoPaterno;
    }

    /**
     * @return mixed
     */
    public function getApellidoMaterno()
    {
        return $this->_apellidoMaterno;
    }

    /**
     * @param mixed $_apellidoMaterno
     */
    public function setApellidoMaterno($_apellidoMaterno)
    {
        $this->_apellidoMaterno = $_apellidoMaterno;
    }



}