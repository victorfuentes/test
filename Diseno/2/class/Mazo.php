<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 26-07-15
 * Time: 12:44 AM
 */

class Mazo {

    /**
     * Corresponde a la cantidad de cartas a entregar
     * @var int
     */
    private $_cantidadCartasEntregar;
    /**
     * Corresponde a la cantidad total de cartas en el mazo
     * @var int
     */
    private $_cantidadTotalCartas;
    /**
     * Corresponde a un array de objetos Carta
     * @var array
     */
    private $_mazo;
    /**
     * Corresponde a un array de objetos Carta a entregar del array $this->_mazo
     * @var array
     */
    private $_cartasEntregar;

    /**
     * Constructor Clase Mazo
     *
     * @param array $mazo
     */
    function __construct(array $mazo)
    {
        $this->_cantidadCartasEntregar=0;
        $this->_mazo=$mazo;
        $this->_cantidadTotalCartas=count($this->_mazo);
        $this->_cartasEntregar=array();
    }

    /**
     * Devuelve un array de objetos Carta desordenado, corresponde al mazo
     * @return array
     *
     */
    public function barajarCartas(){
        shuffle($this->_mazo);
        $this->_cantidadTotalCartas=count($this->_mazo);
        return $this->_mazo;

    }

    /**
     * Devuelve un array de objetos Carta el cual corresponde al mazo
     * @return array
     */
    public function getMazo(){
        return $this->_mazo;
    }

    /**
     * Cambia el array mazo
     * @param array $mazo
     */
    public function setMazo(array $mazo){
        $this->_mazo=$mazo;
    }

    /**
     * Entrega cartas, según el parametro
     * @param $cantidadEntregar
     * Indica la cantidad de cartas a entregar
     * @return array|bool
     * Retorna un array de objetos Carta , si no quedan más cartas devuelve un false.
     */
    public function entregarCartas($cantidadEntregar)
    {
        $this->_cartasEntregar=array();
        if( $this->getCantidadTotalCartas() > 0 ) {
            for( $i=0;$i<$cantidadEntregar;$i++ ) {
                $posCartaEntregar=mt_rand(0,$this->getCantidadTotalCartas());
                $this->__unset($posCartaEntregar);
                $this->_cartasEntregar[]=$this->_mazo[$posCartaEntregar];
            }
            $this->_cantidadTotalCartas=count($this->_mazo);
            return $this->_cartasEntregar;

        }else{
            return false;
        }
    }

    /**
     * Vuelve a iniciar una ronda, cambia el valor del array mazo y el total de cartas en el mazo.
     * @param array $mazo
     * Requiere un nuevo array con objetos Carta.
     */
    public function volverIniciar(array $mazo){
        $this->_mazo=$mazo;
        $this->_cantidadTotalCartas=count($this->_mazo);

    }

    /**
     * Elimina del mazo la carta entregada según la posición
     * @param $posCartaEntregar
     * Parametro que indica la posición de la carta a entregar
     */
    public function __unset($posCartaEntregar)
    {
        unset($this->_mazo[$posCartaEntregar]);
        $this->_mazo=array_values($this->_mazo);
    }

    /**
     * Devuelve la cantiadad de cartar a entregar
     * @return int
     */
    public function getCantidadCartasEntregar()
    {
        return $this->_cantidadCartasEntregar;
    }

    /**
     * Devuelve la cantidad total de cartas en el mazo.
     * @return int
     */
    public function getCantidadTotalCartas()
    {
        return $this->_cantidadTotalCartas;
    }
}