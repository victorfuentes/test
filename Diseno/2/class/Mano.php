<?php
/**
 * Created by PhpStorm.
 *
 * Clase Mano
 *
 * User: Victor
 * Date: 26-07-15
 * Time: 12:44 AM
 */

class Mano {
    /**
     * @var int
     */
    private $_cantidadCartasMano;
    /**
     * @var int
     */
    private $_cantidadCartasMaxMano;
    /**
     * @var int
     */
    private $_cantPuntos;
    /**
     *  Array que contiene objetos de tipo Carta
     * @var  array
     */
    private $_cartasEnMano ;

    /**
     * Constructor para la clase Mano
     *
     * @param $cantidadCartasMaxMano
     * Indica la cantidad máxima que puede tener en la mano, si se inicia en 0 la cantidad el ilimitada.
     * @param $cartasEntregadas
     * Corresponde al un array con objetos de cartas entregadas
     */
    function __construct($cantidadCartasMaxMano,$cartasEntregadas)
    {
        $this->_cantidadCartasMaxMano=$cantidadCartasMaxMano;
        $this->_cartasEnMano=$cartasEntregadas;
        $this->_cantCartasMano=count($this->_cartasEnMano);
        $this->_cantPuntos=0;
    }

    /**
     * Permite agregar o quitar cantidad de cartas de la mano, devolviendo true si fue posible o false si no quedan
     * más cartas en la mano.
     * @return bool
     */
    public function agregarCartaMano(){
        $this->_cantCartasMano=count($this->_cartasEnMano);
        if($this->_cantidadCartasMaxMano==0)
        {
            $this->_cantidadCartasMano++;
            return true;
        }elseif($this->_cartasEnMano <= $this->_cantidadCartasMaxMano )
        {
            $this->_cantidadCartasMano++;
            return true;
        }else{
            return false;
        }
    }

    /**
     * Función que permite botar una carta de la mano.
     * @param $posCartaBotar int
     * Corresponde a la posición del objeto carta a botar que está en el array $this->_cartasEnMano
     */
    public function botarCarta($posCartaBotar){
        $this->__unset($posCartaBotar);
        $this->_cantidadCartasMano=count($this->_cartasEnMano);
    }

    /**
     * Corresponde a la posición de la carta a botar
     * @param $posCartaBotar int
     */
    public function __unset($posCartaBotar)
    {
        unset($this->_cartasEnMano[$posCartaBotar]);
        $this->_cartasEnMano=array_values($this->_cartasEnMano);
    }

    /**
     * Devuelve la cantidad de puntos que suman las cartas en la mano
     * @return int
     */
    public function getCantidadPuntos(){
        foreach($this->_cartasEnMano as $carta)
        {
         $this->_cantPuntos+=$carta->getPuntos();
        }
        return $this->getCantidadPuntos();
    }

    /**
     * Retorna un array con objetos Carta
     * @return array
     */
    public function getCartasEnMano(){
        return $this->_cartasEnMano;
    }

    /**
     * Cambia las cartas que están en la mano
     * @param array $cartasEnMano
     * Corresponde a un array de objetos Carta
     */
    public function setCartasEnMano(array $cartasEnMano){
        $this->_cartasEnMano=$cartasEnMano;
    }

    /**
     * Devuelve la cantidad de cartas que se tienen en la mano.
     * @return int
     */
    public function getCantidadCartasMano()
    {
        return $this->_cantidadCartasMano;
    }

    /**
     * Devuelve la cantidad máxima de cartas que se pueden tener en la mano.
     * @return int
     */
    public function getCantidadCartasMaxMano()
    {
        return $this->_cantidadCartasMaxMano;
    }
}