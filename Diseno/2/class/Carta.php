<?php
/**
 * Created by PhpStorm.
 *
 * Clase Carta
 *
 *
 * User: Victor
 * Date: 26-07-15
 * Time: 12:44 AM
 */

class Carta {
    /**
     * Numero de la carta.
     * @var int
     */
    private $numero;
    /**
     * Puntos de la carta.
     * @var int
     */
    private $puntos;
    /**
     * Nombre de la carta, en caso de tenerlo.
     * @var string
     */
    private $nombre;

    /**
     * Devuelve el número de la carta
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Cambia el número de la carta.
     * @param int $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * Devuelve los puntos de la carta.
     * @return int
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Cambia el número de la carta.
     * @param mixed $puntos
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;
    }

    /**
     * Devuelve el nombre la carta, en caso de tener.
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Cambia el nombre la carta
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }


}