/**
 * Created by Victor on 26-07-15.
 */

$(document).ready(function(){

    dias=["lunes","martes","mieroles","jueves","viernes"];
    wcol=(960/(dias.length + 1))-2;
    wcos=(wcol/2)-2;

    $("#head").append("<div class='bloqD mark' style='width: "+(wcos)+"px;'><i class='fa fa-angle-double-left'></i></i> </div>");
    for(var i=0;i<dias.length;i++) {
        $("#head").append("<div class='bloqD dias' style='width: "+wcol+"px;'>"+dias[i]+"</div>");
    }
    $("#head").append("<div class='bloqD mark' style='width: "+(wcos)+"px;'><i class='fa fa fa-angle-double-right'></i> </div>");
    hh=00;
    mm=00;
    for(var i=1;i<=48;i++) {
        $("#bloqs").append("<div  style='clear:both' id='h-"+i+"' ></div>");
        txthora="";
        if(hh<10){
            txthora+="0";
        }
        txthora+=hh+":"+mm;
        if(mm==0){
            txthora+="0";
        }

        $("#h-"+i).append("<div class='bloqD hora'  style='width: "+(wcos)+"px;'>"+txthora+"</div>");
        for(var j=0;j<dias.length;j++) {
            oc=0;
            if(citas[dias[j]]) {
                datg=citas[dias[j]];
                for (var k = 0; k < datg.length; k++) {
                    dat = datg[k];
                    if (dat['hora_inicio'] <= txthora && dat['hora_termino'] > txthora) {
                        //alert(txthora + " - " + dat['nombre']+" "+dias[j]);
                        nameShow=dat["nombre"].substr(0,15);
                        $("#h-"+i).append("<div title='Paciente: "+dat["nombre"]+"' class='bloqD ocupada' style='width:"+wcol+"px;background: lightgray' >" +
                        "<i class='fa fa-calendar'></i>"+nameShow+"</div>");
                        oc=1;
                    }

                }
            }
            if(oc==0) {
                $("#h-" + i).append("<div class='bloqD' title='Agregar cita' style='width:"+wcol+"px;'><i class='fa fa-plus-circle'></i></div>");
            }
        }
        if(i%2==0)
        {
            hh++;
            mm=0;
        }else{
            mm+=30;
        }
        $("#h-"+i).append("<div class='bloqD hora'  style='width: "+(wcos)+"px;'>"+txthora+"</div>");
    }
});