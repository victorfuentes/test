SELECT a.*,c.idcurso,c.nombrecurso,avg(nota) as prom
FROM pruebas p
  JOIN resultados r on (r.id_prueba=p.idprueba)
  JOIN alumnos a on(a.idalumno=r.id_alumno)
  JOIN cursos_alumnos ca on (ca.idalumno=a.idalumno)
  JOIN cursos c on (c.idcurso=ca.idcurso)
WHERE p.idcurso = c.idcurso
GROUP by c.idcurso,a.idalumno
ORDER BY c.idcurso;