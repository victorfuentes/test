SELECT a.*,AVG(r.nota) as promedio
FROM pruebas p
  JOIN resultados r on (r.id_prueba=p.idprueba)
  JOIN alumnos a on(a.idalumno=r.id_alumno)
  JOIN cursos_alumnos ca on (ca.idalumno=a.idalumno)
  JOIN cursos c on (c.idcurso=ca.idcurso)
  WHERE c.idcurso=3 and a.idalumno=1;