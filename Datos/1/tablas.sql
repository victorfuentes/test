CREATE TABLE alumnos
(
    idalumno INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nombresalumno VARCHAR(100),
    apaternoalumno VARCHAR(100),
    amaternoalumno VARCHAR(100)
);
CREATE TABLE cursos
(
    idcurso INT NOT NULL AUTO_INCREMENT,
    nombrecurso VARCHAR(100),
    idprofesor INT NOT NULL,
    PRIMARY KEY (idcurso, idprofesor)
);
CREATE TABLE cursos_alumnos
(
    idcurso INT NOT NULL,
    idalumno INT NOT NULL,
    PRIMARY KEY (idcurso, idalumno)
);
CREATE TABLE profesores
(
    idprofesor INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nombresprofesor VARCHAR(100),
    apaternoprofesor VARCHAR(100),
    amaternoprofesor VARCHAR(100)
);
CREATE TABLE pruebas
(
    idprueba INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nombreprueba VARCHAR(100),
    idcurso INT NOT NULL
);
CREATE TABLE resultados
(
    idresultado INT NOT NULL AUTO_INCREMENT,
    id_alumno INT NOT NULL,
    id_prueba INT NOT NULL,
    nota VARCHAR(5),
    PRIMARY KEY (idresultado, id_prueba, id_alumno)
);
ALTER TABLE cursos ADD FOREIGN KEY (idprofesor) REFERENCES profesores (idprofesor);
CREATE INDEX fk_cursos_profesores1_idx ON cursos (idprofesor);
ALTER TABLE cursos_alumnos ADD FOREIGN KEY (idalumno) REFERENCES alumnos (idalumno);
ALTER TABLE cursos_alumnos ADD FOREIGN KEY (idcurso) REFERENCES cursos (idcurso);
CREATE INDEX cursos_alumno_alumnos_idx ON cursos_alumnos (idalumno);
CREATE INDEX cursos_alumno_cursos_idx ON cursos_alumnos (idcurso);
ALTER TABLE resultados ADD FOREIGN KEY (id_alumno) REFERENCES alumnos (idalumno);
ALTER TABLE resultados ADD FOREIGN KEY (id_prueba) REFERENCES pruebas (idprueba);
CREATE INDEX resultados_alumnos_idx ON resultados (id_alumno);
CREATE INDEX resultados_pruebas_idx ON resultados (id_prueba);
