SELECT alum.idalumno,alum.nombresalumno,alum.apaternoalumno,alum.apaternoalumno, count(res.prom) AS promrojos
FROM (
   SELECT a.idalumno, avg(nota) AS prom
     FROM pruebas p
      JOIN resultados r      ON (r.id_prueba = p.idprueba)
      JOIN alumnos a         ON (a.idalumno = r.id_alumno)
      JOIN cursos_alumnos ca ON (ca.idalumno = a.idalumno)
      JOIN cursos c          ON (c.idcurso = ca.idcurso)
   WHERE p.idcurso = c.idcurso
   GROUP BY c.idcurso, a.idalumno
   HAVING avg(nota) < 4
    ) res
 JOIN alumnos alum ON (res.idalumno = alum.idalumno)
GROUP BY res.idalumno
HAVING count(res.prom) > 1