<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 26-07-15
 * Time: 05:14 AM
 */

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

$numT=$_POST["num"];
numberToText($numT);

/**
 * @param $numT
 * @return string
 */

function numberToText($numT) {

    $u=array("cero", "uno"    ,"dos"     ,"tres"    ,"cuatro"    ,"cinco"     ,"seis"       ,"siete"     ,"ocho"      ,"nueve");
    $d=array("diez","once" ,"doce"   ,"trece"   ,"catorce" ,"quince"    ,"dieciseis" ,"diecisiete" ,"dieciocho" ,"diecinueve");
    $d2=array("","","veinte","treinta" ,"cuarenta","cincuenta" ,"sesenta"   ,"setenta"    ,"ochenta"   ,"noventa");
    $cientosSp=array(1,9,5);
    $c=array("cientos",array("","ciento","","","" ,"quinientos","","","","novecientos"));
    $m=array(1=>"mi"    ,2=>"bi"    ,3=>"tri"     ,4=>"cuatri"    ,5=>"quinti"    ,6=>"sexti"     ,7=>"septi"       ,8=>"octi"     ,9=>"noni"    , 10=>"deci",11=>"undeci",12=>"dudeci",13=>"tetradeci",14=>"pentadeci");
    //$num=number_format($numT,0,"",".");
    $num=Number_Format2($numT,0,"",".");
    $arrayNum=explode(".",$num);
    $temp="";
    $inv=$cantGroup=count($arrayNum);
    $ini=1;
    for($i=3;$i<=$cantGroup;$i++) {
        $arrayMill[$i]=$ini;
        if($i%2==0) {
            $ini++;
        }
    }
    foreach($arrayNum as $n) {
        if ($inv > 2) {
            $pos = $arrayMill[$inv];
        }
        if($inv%2==0)
        {
            if(count($n)==1 && $n==1){
                $temp .= " mil ";
            }else{
                if($n==100)
                {
                    $temp .="cien"." mil ";
                }else{
                    $temp .= txtN($n,$cientosSp,$c,$u,$d,$d2)." mil ";
                }

            }

        }else{
            if($inv>2) {
                if(count($n)==1 && $n==1){
                    $txt="llon ";
                }else{
                    $txt="llones ";
                }
                if($n==100)
                {
                    $temp .="Cien"." ".$m[$pos].$txt;
                }else{
                    $temp .= txtN($n,$cientosSp,$c,$u,$d,$d2)." ".$m[$pos].$txt;
                }

            }else{
                if($inv==1 && $n==1 )
                {
                    $temp .= "uno";
                }elseif($inv==1 && $n==100) {
                    $temp .= "cien";
                }else{
                    $temp .= txtN($n,$cientosSp,$c,$u,$d,$d2);
                }

            }
        }
        $inv--;
    }
    echo ucfirst($temp);
}

/**
 * @param $txt
 * @param $cientosSp
 * @param $ci
 * @param $u
 * @param $d
 * @param $d2
 * @return string
 */

function txtN($txt,$cientosSp,$ci,$u,$d,$d2) {
    $fi=false;
    $se=false;
    $tr=false;
    $temp = "";
    $n = str_split($txt);
    $count = count($n);
    $e = 1;
    if ($count == 3) {
        $fi = $n[0];
        $se = $n[1];
        $tr = $n[2];
    }elseif($count==2) {
        $se = $n[0];
        $tr = $n[1];
    }elseif($count==1) {
        $tr = $n[0];
    }
    if(in_array($fi,$cientosSp) && $fi!=0 && $fi) {
        $temp.=$ci[1][$fi]." ";
    }elseif($fi) {
        $temp.=$u[$fi].$ci[0]." ";
    }
    if( $se!=0 && $se) {
        if($se==1) {
            $temp .= $d[$tr] . " ";
            $e=false;
        }else
            $temp .= $d2[$se] . " ";
    }
    if( $tr!=0 && $e && $tr) {
        if($tr==1 && $count==1) {
            $temp .= "un";
        }else {
            if($count!=1) {
                $temp .= "y ";
            }
            $temp .= $u[$tr];
        }
    }
    return $temp;
}


/**
 * @param $number
 * @param int $decimal_precision
 * @param string $decimals_separator
 * @param string $thousands_separator
 * @return array|string
 */

function Number_Format2($number, $decimal_precision = 0, $decimals_separator = '.', $thousands_separator = ',')
{

    $number = explode('.', str_replace(' ', '', $number));

    $number[0] = str_split(strrev($number[0]), 3);

    $total_segments = count($number[0]);

    for ($i = 0; $i < $total_segments; $i++)
    {
        $number[0][$i] = strrev($number[0][$i]);
    }

    $number[0] = implode($thousands_separator, array_reverse($number[0]));

    if (!empty($number[1]))
    {
        $number[1] = $this->Round($number[1], $decimal_precision);
    }

    $number = implode($decimals_separator, $number);

    return $number;
}