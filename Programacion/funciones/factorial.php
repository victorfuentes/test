<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 26-07-15
 * Time: 05:08 AM
 */


/**
 * @param $num int
 * @return int
 */
$num=$_POST["num"];
zeroFewToTheRight($num);
function zeroFewToTheRight($num){
    $acum=0;
    for($i=1;$i<=$num;$i++)
    {
        $n=5;
        if($i%5==0)
        {
            $acum++;
        }
        while($n<$num)
        {
            $n=$n*5;
            if($n<=$i) {
                if ($i % $n == 0) {
                    $acum++;
                }
            }else{
                $n=$num+1;
            }
        }
    }
    echo $acum;
}