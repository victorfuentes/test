<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 26-07-15
 * Time: 05:08 AM
 */
?>
<script type="text/javascript" src="rec/jquery-1.11.3.min.js"></script>
<script>
    $(document).ready( function(){
        $("#showZero").hide();
        $("#showTxt").hide();

        $("#calc").click( function(){
            $("#showZero").html("Calculando...");
            var num=$("#factoNum").val();
            $.ajax({
                type:"POST",
                url:"funciones/factorial.php",
                data:{"num":num}
            }).done(function(res){
                $("#showZero").show();
                $("#showZero").html(res);
            });

        });

        $("#calcTxt").click( function(){
            $("#showTxt").html("Calculando...");
            var num=$("#txtNum").val();
            $.ajax({
                type:"POST",
                url:"funciones/numPalabra.php",
                data:{"num":num}
            }).done(function(res){
                $("#showTxt").show();
                $("#showTxt").html(res);
            });

        });
    });
</script>

<form action="#" id="cfactorial">1-. La cantidad de 0 del factorial de: <input type="number" id="factoNum"><input type="submit" value="Calcular" id="calc">
    <div id="showZero">es: </div>
</form>
<br>
<br>
Número a texto
<form action="#" id="cTxt">2-. Ingreso un número: <input type="number" id="txtNum"><input type="submit" value="Calcular" id="calcTxt">
    <div id="showTxt">es: </div>
</form>

<br>

3-. Ajedrez <br>

Función incompleta, funciones/ajedrez.php

